<?php
/**
 * WP Default Custom Post Types Sample
 *
 * @package WP_Default
 */

/**
 * Create fence post type
 */
function fence_post_type() {

	$labels = array(
		'name'					=> __( 'Fences', 'mandovifoods' ),
		'singular_name'			=> __( 'Fence', 'mandovifoods' ),
		'add_new'				=> __( 'New Fence', 'mandovifoods' ),
		'add_new_item'			=> __( 'Add New Fence', 'mandovifoods' ),
		'edit_item'				=> __( 'Edit Fence', 'mandovifoods' ),
		'new_item'				=> __( 'New Fence', 'mandovifoods' ),
		'view_item'				=> __( 'View Fence', 'mandovifoods' ),
		'search_items'			=> __( 'Search Fences', 'mandovifoods' ),
		'not_found'				=>  __( 'No Fences Found', 'mandovifoods' ),
		'not_found_in_trash'	=> __( 'No Fences found in Trash', 'mandovifoods' ),
	);
	$args = array(
		'labels'		=> $labels,
		'has_archive'	=> true,
		'public'		=> true,
		'hierarchical'	=> false,
		'menu_icon'		=> 'dashicons-portfolio',
		'rewrite'		=> array( 'slug' => 'fence' ),
		'supports'		=> array(
			'title',
			'editor',
			'excerpt',
			'custom-fields',
			'thumbnail',
			'page-attributes'
		),
		'taxonomies'	=> array( 'post_tag' ),
	);
	register_post_type( 'fence', $args );

}
add_action( 'init', 'fence_post_type' );

/**
 * Create fence post type taxonomy
 */
function register_fence_taxonomy() {

	$labels = array(
		'name'				=> __( 'Categories', 'mandovifoods' ),
		'singular_name'		=> __( 'Category', 'mandovifoods' ),
		'search_items'		=> __( 'Search Categories', 'mandovifoods' ),
		'all_items'			=> __( 'All Categories', 'mandovifoods' ),
		'edit_item'			=> __( 'Edit Category', 'mandovifoods' ),
		'update_item'		=> __( 'Update Category', 'mandovifoods' ),
		'add_new_item'		=> __( 'Add New Category', 'mandovifoods' ),
		'new_item_name'		=> __( 'New Category Name', 'mandovifoods' ),
		'menu_name'			=> __( 'Categories', 'mandovifoods' ),
	);

	$args = array(
		'labels'			=> $labels,
		'hierarchical'		=> true,
		'sort'				=> true,
		'args'				=> array( 'orderby' => 'term_order' ),
		'rewrite'			=> array( 'slug'    => 'fence' ),
		'show_admin_column'	=> true
	);

	register_taxonomy( 'fence_cat', array( 'fence' ), $args);

}
add_action( 'init', 'register_fence_taxonomy' );