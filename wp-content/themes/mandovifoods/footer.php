<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Mandovi_Foods
 * @since 1.0.0
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info container">
			<div class="footer-nav">
				<?php get_template_part( 'template-parts/navigation/navigation', 'bottom' ); ?>
			</div>
			<?php get_template_part( 'template-parts/footer/site', 'info' ); ?>

		</div><!-- .site-info -->
		<p class="silver"><img src="<?php echo site_url()?>/wp-content/uploads/2019/09/sc-logo.png" alt="" class="company-logo" /> <a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank">Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external" target="_blank">Silver Connect Web Design</a></p>
	</footer><!-- #colophon -->
	
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
