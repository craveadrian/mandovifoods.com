<?php
/**
 * Displays the post header
 *
 * @package WordPress
 * @subpackage Mandovi_Foods
 * @since 1.0.0
 */

$discussion = ! is_page() && mandovifoods_can_show_post_thumbnail() ? mandovifoods_get_discussion_data() : null; ?>

<?php if( !is_home() && !is_front_page() ): ?>
 <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
<?php endif; ?>
<?php if ( ! is_page() ) : ?>
<div class="entry-meta">
	<?php mandovifoods_posted_by(); ?>
	<?php mandovifoods_posted_on(); ?>
	<span class="comment-count">
		<?php mandovifoods_comment_count(); ?>
	</span>
	<?php
	// Edit post link.
		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers. */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'mandovifoods' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<span class="edit-link">' . mandovifoods_get_icon_svg( 'edit', 16 ),
			'</span>'
		);
	?>
</div><!-- .meta-info -->
<?php endif; ?>
