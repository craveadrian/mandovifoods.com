<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'betascxs_mandovifoods' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'R>09Q_2g+n|2,+OS+5OD>m1wUe/:a0Y*e%qbNE!,4!9e07u|$ur&nP@_>=#ekz]_' );
define( 'SECURE_AUTH_KEY',  'oq=&~)QJEf9NS.Yw%fL0.SWpE kR9{JPq!A78xO2B VLS5HK3li]/^R$ |}ZQ]2c' );
define( 'LOGGED_IN_KEY',    '&DK{&++ZU9t^ovT`KNncAyLe!WUYC8kWaOSY{Tfvc>V*^8v2pSx/fz_> &1wR/20' );
define( 'NONCE_KEY',        'ZFiVZptdC+n5|5SXa7:IIPx*I|O+P_N rnB,<4|VSESgr}C8^:r$*Go,G@W#Y>* ' );
define( 'AUTH_SALT',        'g]Fa2v#?OY59?>g?nkz#}a>g-iP<}1` ,dsVgLEf07sEj!KRtUj={Ieveu6(-7fs' );
define( 'SECURE_AUTH_SALT', 'Y[KhV./U8|XLy*ti#)b^Lh:gpPS57d]4Z0vQKCr90c0I-yb{e9kn8l}^*{t?QY>!' );
define( 'LOGGED_IN_SALT',   '|hEo*Q*sWkq1]Wwo]ZEf=C 7c7TdN0T[:N1Tx_@V{>h!qS_f<AGm`1-R%]klV4kM' );
define( 'NONCE_SALT',       'm_+YQ>pEkT%UF(`=bt-+!cib5QurMz|~[yl32Fd1R^LA@@@-IQ%XuyBuFi@R<HM4' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'betascxs_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
